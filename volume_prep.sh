#!/bin/bash

# Enforce that this is not being run by the superuser and exit with 1 if it is
if [[ "$UID" -eq 0 ]]
	then
		echo "Please don't run this as the root user or sudo"
		exit 1
fi

# Set directory to the one containing this script
cd `dirname $0`

# Use environmental variables from .env as defined in docker-compose.yml
source ./.env

# Ensure the directory defined in .env as the bind9 directory exists
if [ ! -d "${BIND9_LOC}" ]
then
	mkdir "${BIND9_LOC}"
fi

# git clone the repo defined in .env into the config directory
cd "${BIND9_LOC}"
git clone "git@${GIT_HOST}:${GIT_USER}/${BIND9_CONFIG_REPO}.git"
mv "${BIND9_CONFIG_REPO}" config/
