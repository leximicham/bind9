#!/bin/bash

# Set the confirmation variable now to be populated later
CONFIRMATION=

# Create a log function which sends specified messages to standard error
log() {
  local MESSAGE="${@}"
  echo "${MESSAGE}" >&2
}

if_fail() {
  if [[ $? -ne 0 ]]
    then
      local MESSAGE2="${@}"
      log "$MESSAGE2"
      log "Aborting"
      exit 1
  fi
}

# Provides a usage statement if the user doesn't supply the right arguments
usage() {
  log "Usage: ${0} [-y]"
  log "Preps local machine to host a DNS server by telling systemd-reserved to stop using port 53"
  log "  -y    Confirm that this script is allowed to reboot the machine"
  exit 1
}

# This script will add users to the local system
log "This script will check if systemd-reserved is using port 53 and then kindly ask it to stop"

# Enforce that this is being run by the superuser and exit with 1 if not
if [[ "$UID" -ne 0 ]]
  then
    log "Please run this as the root user or sudo"
    exit 1
fi

# Check the args of the script and if there are any kindly ask the user to stop
while getopts y OPTION
do
  case ${OPTION} in
    y)
      CONFIRMATION=y
      ;;
    ?)
      usage
      ;;

  esac
done

# Check if anything is actually listening on tcp4 0.0.0.0:53 and verify if it's systemd-reserved then change its config
lsof -i :53 &> /dev/null
if_fail "Port 53 is not currently in use, running this script is unnecessary."

INTERLOPER=$(lsof -i :53 | grep LISTEN | awk '{print $1}')

if [[ $INTERLOPER != "systemd-r" ]]
  then
    log "The process using port 53 is not systemd-received"
    log "Initiate panic"
    log "The port 53 interloper is ${INTERLOPER} - please do the needful"
    log "Aborting"
    exit 1
  else
    log "systemd-received is confirmed to be the port 53 interloper"
    log "Kindly asking it to stop by updating its config to not run a DNS stub listener"
    echo DNSStubListener=no >> /etc/systemd/resolved.conf
    if_fail "Editing /etc/systemd/resolved.conf failed - please check the files permissions and try again"
fi

# Swaps out a nameserver config which points to localhost with one which points to the gateway
log "Swapping /etc/resolv.conf out for /run/systemd/resolve/resolv.conf"
log "This will stop the machine from referring to localhost for DNS queries"
ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
if_fail "Creating the symlink from /run/systemd/resolve/resolv.conf to /etc/resolv.conf failed - please check the file permissions and try again"

# A reboot is now required. Checks with the user before running the command
log "Reboot required to restart systemd"
if [[ -z $CONFIRMATION ]]
  then
    read -p 'Would you like to reboot now? y or n?' CONFIRMATION
fi
if [[ $CONFIRMATION = "y" ]]
  then
    log "Rebooting machine now"
    shutdown -r now
    exit 0
  else
    log "Please reboot this machine at your earliest convenience to prepare system prep for the DNS server docker image"
    exit 0
fi
